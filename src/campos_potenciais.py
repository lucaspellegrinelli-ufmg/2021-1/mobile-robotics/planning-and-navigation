import numpy as np
import matplotlib.pyplot as plt

def rep_force(q, obs, R=1000000, krep=0.001):
  # Obstáculo: (x, y, r)
  v = q - obs[0:2]
  d = np.linalg.norm(v, axis=1) - obs[2]
  d = d.reshape((len(v) ,1))
  
  rep = (1/d**2)*((1/d)-(1/R))*(v/d)    
  
  invalid = np.squeeze(d > R)
  rep[invalid, :] = 0
  
  return krep*rep

def att_force(q, goal, katt=.005):
  return katt * (goal - q)

def create_rep_forces(all_objects, goal, step=0.1):
  WORLD_MIN = -5 - 0.5
  WORLD_MAX = 5 + 0.5

  XX, YY = np.meshgrid(np.arange(WORLD_MIN, WORLD_MAX, step), np.arange(WORLD_MIN, WORLD_MAX, step))
  XY = np.dstack([XX, YY]).reshape(-1, 2)

  Fatt = att_force(XY, np.array(goal))
  Frep = np.zeros((Fatt.shape[0], 2))

  for obj in all_objects:
    for i in range(10):
      obs = np.array([obj[0], obj[1], 0.001 + i * 0.001])
      Frep += rep_force(XY, obs)
  
  return XX, YY, Fatt + Frep