import sim
import numpy as np

from campos_potenciais import *
from plots import *

cube_handles = []
cube_positions = []

# Normalize angle to the range [-pi,pi)
def normalizeAngle(angle):
  return np.mod(angle+np.pi, 2*np.pi) - np.pi

print ('Program started')
sim.simxFinish(-1) # just in case, close all opened connections
clientID=sim.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to CoppeliaSim

if clientID!=-1:
  print ('Connected to remote API server')

  robotname = 'Pioneer_p3dx'
  returnCode, robotHandle = sim.simxGetObjectHandle(clientID, robotname, sim.simx_opmode_oneshot_wait)    
  
  returnCode, robotLeftMotorHandle  = sim.simxGetObjectHandle(clientID, robotname + '_leftMotor', sim.simx_opmode_oneshot_wait)
  returnCode, robotRightMotorHandle = sim.simxGetObjectHandle(clientID, robotname + '_rightMotor', sim.simx_opmode_oneshot_wait)
    
  returnCode, goalHandle = sim.simxGetObjectHandle(clientID, "Goal", sim.simx_opmode_oneshot_wait) 
  returnCode, final_goal = sim.simxGetObjectPosition(clientID, goalHandle, -1, sim.simx_opmode_oneshot_wait)

  # Pega todas as quintas dos cuboids
  for i in range(25):
    returnCode, cuboid = sim.simxGetObjectHandle(clientID, "Cuboid" + str(i), sim.simx_opmode_oneshot_wait)
    _, pos = sim.simxGetObjectPosition(clientID, cuboid, -1, sim.simx_opmode_oneshot_wait)
    if returnCode == 0 and cuboid not in cube_handles and cuboid != 0:
      cube_handles.append(cuboid)
      cube_positions.append(pos)

  final_goal = np.array([final_goal[0], final_goal[1]])

  XX, YY, forces = create_rep_forces(cube_positions, np.array(final_goal))
  plot_potential(XX, YY, forces)
  XY = np.dstack([XX, YY]).reshape(-1, 2)
 
  # Específico do robô
  L = 0.331
  r = 0.09751
  maxv = 1.0
  maxw = np.deg2rad(45)
  
  rho = np.inf
  while True:
    returnCode, robotPos = sim.simxGetObjectPosition(clientID, robotHandle, -1, sim.simx_opmode_oneshot_wait)
    returnCode, robotOri = sim.simxGetObjectOrientation(clientID, robotHandle, -1, sim.simx_opmode_oneshot_wait)        
    robotConfig = np.array([robotPos[0], robotPos[1], robotOri[2]])
    
    best_i = -1
    best_dist = 99999
    for i, mesh_pos in enumerate(XY):
      d = np.hypot(mesh_pos[0] - robotPos[0], mesh_pos[1] - robotPos[1])
      if d < best_dist:
        best_dist = d
        best_i = i

    qgoal = np.array([robotPos[0] + forces[best_i][0], robotPos[1] + forces[best_i][1], np.deg2rad(90)])

    dx, dy, dth = qgoal - robotConfig
    
    rho = np.sqrt(dx**2 + dy**2) * 30
    alpha = normalizeAngle(-robotConfig[2] + np.arctan2(dy,dx))
    beta = normalizeAngle(qgoal[2] - np.arctan2(dy,dx))
    
    kr = 4 / 20
    ka = 8 / 20
    kb = -1.5 / 20
    
    # Alvo na parte de trás
    if abs(alpha) > np.pi/2:
      kr = -kr       
      
      # Se não ajustar a direção muda
      alpha = normalizeAngle(alpha-np.pi)
      beta = normalizeAngle(beta-np.pi)
    
    v = kr*rho
    w = ka*alpha + kb*beta
    
    # Limit v,w to +/- max
    v = max(min(v, maxv), -maxv)
    w = max(min(w, maxw), -maxw)        
    
    wr = ((2.0*v) + (w*L))/(2.0*r)
    wl = ((2.0*v) - (w*L))/(2.0*r)
    
    sim.simxSetJointTargetVelocity(clientID, robotRightMotorHandle, wr, sim.simx_opmode_oneshot_wait)
    sim.simxSetJointTargetVelocity(clientID, robotLeftMotorHandle, wl, sim.simx_opmode_oneshot_wait)

  sim.simxSetJointTargetVelocity(clientID, robotRightMotorHandle, 0, sim.simx_opmode_oneshot_wait)
  sim.simxSetJointTargetVelocity(clientID, robotLeftMotorHandle, 0, sim.simx_opmode_oneshot_wait)

  # Now close the connection to CoppeliaSim:
  sim.simxFinish(clientID)
    
else:
  print ('Failed connecting to remote API server')
    
print ('Program ended')