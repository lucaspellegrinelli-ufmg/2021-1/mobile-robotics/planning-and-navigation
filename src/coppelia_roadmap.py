import sim
import numpy as np
import math

from grafo_visibilidade import *
from plots import *
from utils import *

cube_size = 0.5
cube_corners = []
cube_handles = []

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

if clientID != -1:
  print ("Connected to remote API server")

  returnCode = 1
  while returnCode != 0:
    returnCode, robo = sim.simxGetObjectHandle(clientID, "robotino", sim.simx_opmode_oneshot_wait)
    returnCode, wheel1 = sim.simxGetObjectHandle(clientID, 'wheel0_joint', sim.simx_opmode_oneshot_wait)
    returnCode, wheel2 = sim.simxGetObjectHandle(clientID, 'wheel1_joint', sim.simx_opmode_oneshot_wait)
    returnCode, wheel3 = sim.simxGetObjectHandle(clientID, 'wheel2_joint', sim.simx_opmode_oneshot_wait)

  returnCode, robo_position = sim.simxGetObjectPosition(clientID, robo, -1, sim.simx_opmode_oneshot_wait)

  # Robotino
  L = 0.135   # Metros
  r = 0.040   # Metros

  # Cinemática Direta
  Mdir = np.array([[-r/np.sqrt(3), 0, r/np.sqrt(3)], [r/3, (-2*r)/3, r/3], [r/(3*L), r/(3*L), r/(3*L)]])

  gain = np.array([[0.1, 0, 0], [0, 0.1, 0], [0, 0, 0.1]])

  # Pega todas as quintas dos cuboids
  for i in range(11):
    returnCode, cuboid = sim.simxGetObjectHandle(clientID, "Cuboid" + str(i), sim.simx_opmode_oneshot_wait)
    _, pos = sim.simxGetObjectPosition(clientID, cuboid, -1, sim.simx_opmode_oneshot_wait)
    if returnCode == 0 and cuboid not in cube_handles and cuboid != 0:
      cube_handles.append(cuboid)
      cube_corners.append([
        (pos[0] + cube_size, pos[1] + cube_size),
        (pos[0] - cube_size, pos[1] + cube_size),
        (pos[0] + cube_size, pos[1] - cube_size),
        (pos[0] - cube_size, pos[1] - cube_size)
      ])

  returnCode, goalHandle = sim.simxGetObjectHandle(clientID, "Goal", sim.simx_opmode_oneshot_wait) 
  returnCode, final_goal = sim.simxGetObjectPosition(clientID, goalHandle, -1, sim.simx_opmode_oneshot_wait)
  
  robo_pos = (robo_position[0], robo_position[1])
  goal_pos = (final_goal[0], final_goal[1])

  all_lines = create_graph(robo_pos, goal_pos, cube_corners)
  path = find_shortest_path(all_lines, robo_pos, goal_pos)
  plot_grafo_visilibidade(all_lines, robo_pos, goal_pos, path, cube_corners)
  
  current_path = 1

  while True:
    qgoal = np.array([path[current_path][0], path[current_path][1], np.deg2rad(90)])

    returnCode, pos = sim.simxGetObjectPosition(clientID, robo, -1, sim.simx_opmode_oneshot_wait)        
    returnCode, ori = sim.simxGetObjectOrientation(clientID, robo, -1, sim.simx_opmode_oneshot_wait)
    q = np.array([pos[0], pos[1], ori[2]])

    error = qgoal - q

    if (np.linalg.norm(error[:2]) < 0.2):
      current_path += 1
      if current_path == len(path):
        break

    # Controller
    qdot = gain @ error

    # Cinemática Inversa
    # w1, w2, w3
    Minv = np.linalg.inv(Rz(q[2]) @ Mdir)
    u = Minv @ qdot

    # Enviando velocidades
    sim.simxSetJointTargetVelocity(clientID, wheel1, u[0], sim.simx_opmode_streaming)
    sim.simxSetJointTargetVelocity(clientID, wheel2, u[1], sim.simx_opmode_streaming)
    sim.simxSetJointTargetVelocity(clientID, wheel3, u[2], sim.simx_opmode_streaming)

  sim.simxSetJointTargetVelocity(clientID, wheel1, 0, sim.simx_opmode_oneshot_wait)
  sim.simxSetJointTargetVelocity(clientID, wheel2, 0, sim.simx_opmode_oneshot_wait)
  sim.simxSetJointTargetVelocity(clientID, wheel3, 0, sim.simx_opmode_oneshot_wait)
else:
  print ("Failed connecting to remote API serve")
print ("Program ended")