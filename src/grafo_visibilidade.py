import networkx as nx

def create_graph(start_pos, end_pos, all_objects):
  all_lines = []

  def ccw(A,B,C):
    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

  def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

  def same_corner(a, b):
    return a[0] == b[0] and b[1] == a[1]

  def check_if_connection_is_clear(curr_corner, link_corner, all_corners):
    for object in all_corners:
      for corner_a in object:
        if same_corner(corner_a, link_corner): continue
        for corner_b in object:
          if same_corner(corner_b, link_corner): continue
          if not same_corner(corner_a, corner_b):
            if intersect(curr_corner, link_corner, corner_a, corner_b):
              return False
    return True

  def check_all_connections(curr_corner, all_corners, object_id):
    for i, object in enumerate(all_corners):
      if i == object_id: continue
      for corner in object:
        if check_if_connection_is_clear(curr_corner, corner, all_corners):
          all_lines.append((
            (curr_corner[0], curr_corner[1]),
            (corner[0], corner[1])
          ))

  for i, object in enumerate(all_objects):
    for corner in object:
      check_all_connections(corner, all_objects, i)

  check_all_connections(start_pos, all_objects, -1)
  check_all_connections(end_pos, all_objects, -1)

  return all_lines

def find_shortest_path(graph_lines, start, end):
  G = nx.Graph()
  G.add_edges_from(graph_lines)
  p = nx.shortest_path(G, source=start, target=end)
  return p

    