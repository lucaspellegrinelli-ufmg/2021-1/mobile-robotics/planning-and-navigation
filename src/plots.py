import matplotlib.pyplot as plt
import numpy as np

def line(A,B,C,D):
  plt.plot([A, C], [B, D], color="black", zorder=0, linewidth=1)

def line2(A,B,C,D):
  plt.plot([A, C], [B, D], color="green", zorder=4, linewidth=3)

def plot_object(object):
  line(object[0][0], object[0][1], object[2][0], object[2][1])
  line(object[1][0], object[1][1], object[3][0], object[3][1])
  line(object[0][0], object[0][1], object[1][0], object[1][1])
  line(object[2][0], object[2][1], object[3][0], object[3][1])

def plot_grafo_visilibidade(all_lines, start_pos, end_pos, path, all_objects):
  for object in all_objects:
    plot_object(object)
    for corner in object:
      plt.scatter(corner[0], corner[1], color="blue", s=50, zorder=2)

  plt.scatter(start_pos[0], start_pos[1], color="green", s=100, zorder=5)
  plt.scatter(end_pos[0], end_pos[1], color="red", s=100, zorder=5)

  for i in range(1, len(path)):
    line2(path[i - 1][0], path[i - 1][1], path[i][0], path[i][1])

  for l in all_lines:
    line(l[0][0], l[0][1], l[1][0], l[1][1])

  plt.xlim(-5, 5)
  plt.ylim(-5, 5)
  plt.gca().set_aspect('equal', adjustable='box')
  plt.show()

def plot_potential(XX, YY, Ft):
  Ft_x = Ft[:,0]
  Ft_y = Ft[:,1]

  fmax = .15
  Fm = np.linalg.norm(Ft, axis=1)
  Ft_x[Fm > fmax], Ft_y[Fm > fmax] = 0, 0

  plt.quiver(XX, YY, Ft_x, Ft_y, color='r')
  plt.xlim(-4, 4)
  plt.ylim(-4, 4)
  plt.gca().set_aspect('equal', adjustable='box')
  plt.show()