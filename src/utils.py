import numpy as np
import matplotlib.pyplot as plt
import math

def Rz(theta):
  return np.array([[np.cos(theta), -np.sin(theta), 0],
                   [np.sin(theta),  np.cos(theta), 0],
                   [0            ,  0            , 1]])

def create_matrix(translation, rotation, scale=1):
  return np.array([[rotation[0][0], rotation[1][0], rotation[2][0], translation[0]],
                   [rotation[0][1], rotation[1][1], rotation[2][1], translation[1]],
                   [rotation[0][2], rotation[1][2], rotation[2][2], translation[2]],
                   [0             ,  0            , 0             , scale]])

def plot_frame(Porg, R, c=["r", "g"]):
  axis_size = 1.0    
  axes = axis_size*R
  x_axis = np.array(axes[0:2,0])
  y_axis = np.array(axes[0:2,1])
  plt.quiver(*Porg[:2], *x_axis, color=c[0], angles="xy", scale_units="xy", scale=1)
  plt.quiver(*Porg[:2], *y_axis, color=c[1], angles="xy", scale_units="xy", scale=1)

def format_laser_data(range_data, scan_range, step_size, max_sensor_range=5):
  laser_data = []
  range_data = np.asarray(range_data)
  pts = math.floor(scan_range/step_size)
  angle =- scan_range * 0.5
  for i in range(pts):
    dist = range_data[i]        
    if dist <= 0:
      dist = max_sensor_range
    laser_data.append([angle, dist])
    angle=angle+step_size
  return np.array(laser_data)
    
def draw_laser_data_persistent(ax, laser_data, t_mat, max_sensor_range=5):
  for (ang, dist) in laser_data:
    if dist < max_sensor_range:
      x = dist * np.cos(ang)
      y = dist * np.sin(ang)
      real_pos = t_mat @ np.array([x, y, 0, 1])
      ax.plot(real_pos[0], real_pos[1], "o", color="r", markersize=1)

def draw_laser_data(laser_data, t_mat, ax=None, max_sensor_range=5):
  fig = plt.figure(figsize=(6, 6), dpi=100)
  ax = fig.add_subplot(111, aspect="equal")

  for (ang, dist) in laser_data:
    if dist < max_sensor_range:
      x = dist * np.cos(ang)
      y = dist * np.sin(ang)
      pos = np.array([x, y, 0, 1])
      real_pos = t_mat @ pos
      ax.plot(real_pos[0], real_pos[1], "o", color="r")

  robot_pos = np.array([0, 0, 0, 1])
  real_robot_pos = t_mat @ robot_pos
  ax.plot(real_robot_pos[0], real_robot_pos[1], "k>", markersize=10)
  ax.grid()
  ax.set_xlim([-max_sensor_range, max_sensor_range])
  ax.set_ylim([-max_sensor_range, max_sensor_range])
  plt.show()